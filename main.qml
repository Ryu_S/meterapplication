import QtQuick 2.12
import QtQuick.Window 2.12

Window {

    visible: true
    width: 640
    height: 480
    title: qsTr("meter app")

    Text {
        id: text_speed
        anchors.centerIn: parent
        text: "speed"
        anchors.verticalCenterOffset: -64
        anchors.horizontalCenterOffset: -108
        font.pixelSize: 40
    }

    Text {
        id: val_speed
        objectName: "val_speed"
        x: 342
        y: 156
        text: meter.speed
        font.pixelSize: 40
    }

    Text {
        id: text_rpm
        anchors.centerIn: parent
        text: "rpm"
        anchors.verticalCenterOffset: 32
        anchors.horizontalCenterOffset: -90
        font.pixelSize: 40
    }

    Text {
        id: val_rpm
        objectName: "val_rpm"
        x: 342
        y: 252
        text: meter.rpm
        font.pixelSize: 40
    }
//    Connections{
//        target: meter
//        onSpeedTimeOut:{
//            val_speed.text=meter.speed
//        }
//    }
}
