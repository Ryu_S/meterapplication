﻿#ifndef METERMODEL_H
#define METERMODEL_H
//#define DEBUG

#include <QObject>
#include <QDebug>
#include <QTimer>

class MeterModel : public QObject
{
    Q_OBJECT

public:
    MeterModel(unsigned short speed=0, unsigned short rpm=0);

    inline unsigned short rawSpeed(){
        return m_raw_speed;
    }
    inline unsigned short rawRpm(){
        return m_raw_rpm;
    }
    void updateSpeed();
    void updateRpm();
    void initUpdateTimer();
    void initDebugTimer();
    void startUpdateTimer();
    void startDebugTimer();
    void startTimer();

signals:
    void speedUpdated(unsigned short m_raw_speed);
    void rpmUpdated(unsigned short m_raw_rpm);

public slots:
    void updateValue();
    void rawValue();

private:
    unsigned short m_raw_speed;
    unsigned short m_raw_rpm;
    short s_mode;
    short r_mode;
    QTimer* m_updateTimer;
    QTimer* m_debugTimer;

    static const int INC = 1;
    static const int DEC = -1;
    static const int SPEED_MIN = 0;
    static const int SPEED_MAX = 200;
    static const int RPM_MIN = 0;
    static const int RPM_MAX = 9000;
    static const int UPDATE_INTERVAL = 16;
    static const int DEBUG_INTERVAL = 200;
};

#endif // METERMODEL_H
