﻿#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "meterModel.h"
#include "meterViewModel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    MeterModel model;
    MeterViewModel view_model(model.rawSpeed(), model.rawRpm());

    model.startTimer();
    view_model.startTimer();

    QObject::connect(&model, SIGNAL(speedUpdated(unsigned short)), &view_model, SLOT(setSpeed(unsigned short)));
    QObject::connect(&model, SIGNAL(rpmUpdated(unsigned short)), &view_model, SLOT(setRpm(unsigned short)));

    engine.rootContext()->setContextProperty("meter", &view_model);
    const QUrl url(QStringLiteral("qrc:/main.qml"));

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    engine.load(url);

    return app.exec();  // イベントループの開始
}
