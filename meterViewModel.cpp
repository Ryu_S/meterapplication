﻿#include "meterViewModel.h"

MeterViewModel::MeterViewModel(unsigned short speed, unsigned short rpm){
    setValue(speed, rpm);
}

void MeterViewModel:: setSpeed(unsigned short speed){
    if(m_speed != speed){
        m_speed = speed;
    }
}

void MeterViewModel:: setRpm(unsigned short rpm){
    if( m_rpm != (rpm - (rpm%100)) ){
        m_rpm = rpm - (rpm%100);
    }
}

void MeterViewModel:: value(){
    qDebug() << "Speed" << speed() << endl
             << "Rpm"   << rpm()   << endl;
}

void MeterViewModel:: setValue(unsigned short speed, unsigned short rpm){
    setSpeed(speed);
    setRpm(rpm);
}

void MeterViewModel:: viewTimerTimeOut(){  // onTimerTimeOut
    emit speedChanged();
    emit rpmChanged();
}

void MeterViewModel:: initViewTimer(){
    m_viewTimer = new QTimer();
    m_viewTimer->setInterval(VIEW_INTERVAL);
    m_viewTimer->setTimerType(Qt::PreciseTimer);
    QObject::connect(m_viewTimer, SIGNAL(timeout()), this, SLOT(viewTimerTimeOut()));
}

void MeterViewModel:: initDebugTimer(){
    m_debugTimer = new QTimer();
    m_debugTimer->setInterval(DEBUG_INTERVAL);
    m_debugTimer->setTimerType(Qt::PreciseTimer);
    QObject::connect(m_debugTimer, SIGNAL(timeout()), this, SLOT(value()));
}

void MeterViewModel:: startViewTimer(){
    m_viewTimer->start();
}

void MeterViewModel:: startDebugTimer(){
    m_debugTimer->start();
}

void MeterViewModel:: startTimer(){
    initViewTimer();
#ifdef DEBUG
    initDebugTimer();
#endif  // DEBUG
    startViewTimer();
#ifdef DEBUG
    startDebugTimer();
#endif  // DEBUG
}
