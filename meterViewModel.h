#ifndef METERVIEWMODEL_H
#define METERVIEWMODEL_H
//#define DEBUG

#include <QObject>
#include <QDebug>
#include <QTimer>

class MeterViewModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(unsigned short speed READ speed NOTIFY speedChanged)
    Q_PROPERTY(unsigned short rpm READ rpm NOTIFY rpmChanged)

public:
    MeterViewModel(unsigned short speed, unsigned short rpm);

    inline unsigned short speed(){
        return m_speed;
    }
    inline unsigned short rpm(){
        return m_rpm;
    }
    void setValue(unsigned short speed, unsigned short rpm);
    void initViewTimer();
    void initDebugTimer();
    void startViewTimer();
    void startDebugTimer();
    void startTimer();

signals:
    void speedChanged();
    void rpmChanged();
    void speedTimeOut();
    void rpmTimeOut();

public slots:
    void value();
    void viewTimerTimeOut();
    void setSpeed(unsigned short speed);
    void setRpm(unsigned short rpm);

private:
    unsigned short m_speed;
    unsigned short m_rpm;
    QTimer* m_viewTimer;
    QTimer* m_debugTimer;

    static const int VIEW_INTERVAL  = 200;
    static const int DEBUG_INTERVAL  = 200;
};

#endif // METERVIEWMODEL_H
