﻿#include "meterModel.h"

MeterModel::MeterModel(unsigned short speed, unsigned short rpm){
    if(SPEED_MAX >= speed){
        m_raw_speed = speed;
    }
    if(RPM_MAX >= rpm){
        m_raw_rpm = rpm;
    }
    s_mode = INC;
    r_mode = INC;
}

void MeterModel:: updateSpeed(){
    if(SPEED_MAX == m_raw_speed){
        s_mode = DEC;
    }
    else if(SPEED_MIN == m_raw_speed){
        s_mode = INC;
    }
    m_raw_speed += s_mode;
    emit speedUpdated(m_raw_speed);
}

void MeterModel:: updateRpm(){
    if(RPM_MAX == m_raw_rpm){
        r_mode = DEC;
    }
    else if(RPM_MIN == m_raw_rpm){
        r_mode = INC;
    }
    m_raw_rpm += r_mode;
    emit rpmUpdated(m_raw_rpm);
}

void MeterModel:: updateValue(){
    updateSpeed();
    updateRpm();
}

void MeterModel:: rawValue(){
    qDebug() << "rawSpeed" << rawSpeed() << endl
             << "rawRpm"   << rawRpm()   << endl;
}

void MeterModel:: initUpdateTimer(){
    m_updateTimer = new QTimer();
    m_updateTimer->setInterval(UPDATE_INTERVAL);
    m_updateTimer->setTimerType(Qt::PreciseTimer);
    QObject::connect(m_updateTimer, SIGNAL(timeout()), this, SLOT(updateValue()));
}

void MeterModel:: initDebugTimer(){
    m_debugTimer = new QTimer();
    m_debugTimer->setInterval(DEBUG_INTERVAL);
    m_debugTimer->setTimerType(Qt::PreciseTimer);
    QObject::connect(m_debugTimer, SIGNAL(timeout()), this, SLOT(rawValue()));
}

void MeterModel:: startUpdateTimer(){
    m_updateTimer->start();
}

void MeterModel:: startDebugTimer(){
    m_debugTimer->start();
}

void MeterModel:: startTimer(){
    initUpdateTimer();
#ifdef DEBUG
    initDebugTimer();
#endif  // DEBUG
    startUpdateTimer();
#ifdef DEBUG
    startDebugTimer();
#endif  // DEBUG
}
